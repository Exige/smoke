#define _USE_MATH_DEFINES
#include "math.h"
#include "glyphs.h"
#include "visualization.h"
#include "CLegend.h"
int glyphtype = Glyph_Lines;

float getVectorLength(float x, float y) {
    return sqrt(x * x + y * y);
}

float toDegrees(float angle) {
    return (180 / M_PI)*angle;
}


void drawVectors(int glyphtype, fftw_real max) {
    
     // Temporarily switch to glortho2d.
    // change the current matrix to PROJECTION
    //glMatrixMode(GL_PROJECTION); 
    
    // get the values from PROJECTION matrix to local variable    
  // double projectmatrix[16]; 
   // glGetDoublev(GL_PROJECTION_MATRIX, projectmatrix); 
    
    // Reset projection matrix.
  //  glLoadIdentity();
    
     // change current matrix to MODELVIEW matrix again
    glMatrixMode(GL_MODELVIEW);
    
    // Get the modelview matrix.
    double modelmatrix[16]; 
    glGetDoublev(GL_MODELVIEW_MATRIX, modelmatrix); 
    
    // Reset modelview matrix.
    glLoadIdentity();
    
    // 2D projection
   //gluOrtho2D(0.0, (GLdouble) winWidth, 0.0, (GLdouble) winHeight);
    
    
    int idx0;
    fftw_real wn = (fftw_real) winWidth / (fftw_real) (DIM + 1); // Grid cell width
    fftw_real hn = (fftw_real) winHeight / (fftw_real) (DIM + 1); // Grid cell heigh  
//    glColor3f(1.0, 1.0, 1.0);

    float scale_x = vec_dim_x / (float) DIM;
    float scale_y = vec_dim_y / (float) DIM;

    fftw_real cell_width_x = (fftw_real) winWidth / (fftw_real) (DIM + 1);
    fftw_real cell_width_y = (fftw_real) winHeight / (fftw_real) (DIM + 1);

    fftw_real gw = (fftw_real) winWidth / (fftw_real) (vec_dim_x + 1); // Grid cell width
    fftw_real gh = (fftw_real) winHeight / (fftw_real) (vec_dim_y + 1); // Grid cell heigh 

	CColorMap *map = g_VectorLegend->colorMap();

    for (int i = 0; i < vec_dim_x; i++) {
        for (int j = 0; j < vec_dim_y; j++) {
            fftw_real wn = (fftw_real) winWidth / (fftw_real) (vec_dim_x + 1); // Grid cell width
            fftw_real hn = (fftw_real) winHeight / (fftw_real) (vec_dim_y + 1); // Grid cell heigh  

            float vec_x = wn * i + wn;
            float vec_y = hn * j + hn;
            int x1 = vec_x / cell_width_x - 1;
            int y1 = vec_y / cell_width_y - 1;


            fftw_real *data_x;
            fftw_real *data_y;
			fftw_real *scalar;

			switch (vector_field) {
				case 0: //Fluid velocity
					data_x = vx;
					data_y = vy;					
					break;
				case 1: //Force field
					data_x = fx;
					data_y = fy;				
					break;
				default:
					break;
			}			

			switch(scalar_field)
			{
			case 0:	// rho
				scalar = rho;
				break;
			case 1:	// velocity
				scalar = velocity;
				break;
			case 2:
				scalar = forcefield;
				break;
			}

			max = findMax(scalar);

            float new_vx = data_x[DIM * y1 + x1];
            float new_vy = data_y[DIM * y1 + x1];

            idx0 = (j * DIM) + i;
           // direction_to_color(new_vx, new_vy, color_dir);			
			float s = scalar[DIM * y1 + x1];
            //set_color(s/max, vectorcolormap);
//			map->setColor(s/max);
            float fac = float(map->width)/(float)map->size();
			glEnable(GL_TEXTURE_1D);
			glBindTexture(GL_TEXTURE_1D, map->id());
			glTexCoord1f((s/max)/fac);
//			printf(" s / max = %f\n", s / max);
            if (glyphtype == Glyph_Lines) 
            {
                glBegin(GL_LINES); //draw velocities
                glVertex2f(wn + (fftw_real) i * wn, hn + (fftw_real) j * hn);
                glVertex2f((wn + (fftw_real) i * wn) + vec_scale * new_vx, (hn + (fftw_real) j * hn) + vec_scale * new_vy);
                glEnd();
            }
            else if (glyphtype == Glyph_Triangles) 
            {
                float p1[2];
                p1[0] = wn + (fftw_real) i * wn;
                p1[1] = hn + (fftw_real) j * hn;

                float p2[2];
                p2[0] = (wn + (fftw_real) i * wn) + vec_scale * new_vx;
                p2[1] = (hn + (fftw_real) j * hn) + vec_scale * new_vy;

                float p3[2];
                p3[0] = fabs(p2[0] - p1[0]);
                p3[1] = fabs(p2[1] - p1[1]);

                float length = getVectorLength(p3[0], p3[1]);

                float ang = toDegrees(atan2(new_vy, new_vx));

                glLoadIdentity();
                glTranslatef(wn + (fftw_real) i * wn, hn + (fftw_real) j * hn, 0.0);
                glRotatef(ang, 0.0, 0.0, 1.0);

                glBegin(GL_TRIANGLES);
                glVertex3f(0.0, 5.0, 0.0f);
                glVertex3f(length, 0.0, 0.0f);
                glVertex3f(0.0, -5.0, 0.0f);
                glEnd();
            }
            else if (glyphtype == Glyph_Arrows) 
            {
                float p1[2];
                p1[0] = wn + (fftw_real) i * wn;
                p1[1] = hn + (fftw_real) j * hn;

                float p2[2];
                p2[0] = (wn + (fftw_real) i * wn) + vec_scale * new_vx;
                p2[1] = (hn + (fftw_real) j * hn) + vec_scale * new_vy;

                float p3[2];
                p3[0] = fabs(p2[0] - p1[0]);
                p3[1] = fabs(p2[1] - p1[1]);

                float sqlength = (3.0 / 4.0) * getVectorLength(p3[0], p3[1]);
                float trlength = (1.0 / 4.0) * getVectorLength(p3[0], p3[1]);

                float ang = toDegrees(atan2(new_vy, new_vx));
                
                glLoadIdentity();
                glTranslatef(wn + (fftw_real) i * wn, hn + (fftw_real) j * hn, 0.0);
                glRotatef(ang, 0.0, 0.0, 1.0);

                glBegin(GL_QUADS);
                glVertex3f(0.0, -2.0, 0.0f);
                glVertex3f(0.0, 2.0, 0.0f);
                glVertex3f(sqlength, 2.0, 0.0f);
                glVertex3f(sqlength, -2.0, 0.0f);
                glEnd();

                glBegin(GL_TRIANGLES);
                glVertex3f(sqlength, 5.0, 0.0f);
                glVertex3f(sqlength + trlength, 0.0, 0.0f);
                glVertex3f(sqlength, -5.0, 0.0f);
                glEnd();
            }
            glDisable(GL_TEXTURE_1D);

        }
    }
        
         // Reset our matrices.    
    //glMatrixMode(GL_PROJECTION); // change current matrix mode to PROJECTION
  // glLoadMatrixd(projectmatrix); // reset
    glMatrixMode(GL_MODELVIEW); // change current matrix mode to MODELVIEW 
   glLoadMatrixd(modelmatrix); // reset
}