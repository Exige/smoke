#define _USE_MATH_DEFINES
#include <string.h>
#include "stdio.h"
//#include <vector.h>
#include "visualization.h"
#include "simulation.h"
#include "glyphs.h"
#include "CLegend.h"




//
// This file contains all the visualization implementations.
//

int winWidth, winHeight; //size of the graphics window, in pixels
int color_dir = 0; //use direction color-coding or not
float vec_scale = 1000; //scaling of hedgehogs
int draw_smoke = 1; //draw the smoke or not
int draw_vecs = 0; //draw the vector field or not
int draw_isolines = 0;
int scalar_col = 0; //method for scalar coloring
int scalar_col_vec = 0; //method for scalar coloring
float SCALE_MAX = 10.0;
float SCALE_MIN = 0.0;
int relativeScale = 1;
float hue;
float saturation = 1;
int g_drawrange;

const int FLUID_DENSITY = 0;
const int FLUID_VELOCITY = 1;
const int FORCE_FIELD = 2;
const int VELOCITY_DIV_FIELD = 3;
const int FORCE_DIV_FIELD = 4;

fftw_real *forcefield;
fftw_real *divergencefield;
fftw_real *velocity;

int vec_dim_x = 50;
int vec_dim_y = 50;

float isorho0 = 0.5;
float g_isorho1 = 0.0;
int g_numisolines = 1;

// Scale clamping limits for clamp height plot mode.
float g_scalelimit;

// Data set used by the height plot
int g_dataset_3d;

float g_3ddepth;

int g_3dscalemode;

// High value for height plots, used in scaling.
float g_high;

vector<vector<Point> > isolines;
float  g_clampscale3d;

// Fov.
float g_fov;

// Enable height plots?
int height_plots;

int g_movemode;

// Zooming in height plot mode.
int g_zoomfactor;

// Workaround for some incorrectly configured Opengl Linux pc's.
#ifdef _STUPID_ERR_
#include <pthread.h>

void junk()
{
    int i;
    i = pthread_getconcurrency();
};
#endif


bool scalarBetween(float v, float vi, float vj)
{
    if ((v >= vi && v < vj) || (v >= vj && v < vi))
    {
        return true;
    }
    return false;
}

void computeIsolines()
{
    int cnt = 0;
    isolines.clear();
    vector<float> isovalues;
    
    if(g_drawrange)
    {
        float incr;
        // Draw a range of isolines.
        if(g_numisolines == 1)
            // Avoid division by zero.
            incr = (g_isorho1 - isorho0) /2;
        else
            incr = (g_isorho1 - isorho0) / (g_numisolines-1);   
        
        float j = isorho0;
        for(int i = 0;i < g_numisolines;i++)
        {
            isovalues.push_back(j);
            j+=incr;
        }
    }
    else
        isovalues.push_back(isorho0);

	for (int i = 0; i < isovalues.size();i++)
	{
		float v = isovalues[i];
        vector<Point>isoline;

        for (int x = 0; x < DIM-1; x++)
        {
            for (int y = 0; y < DIM-1; y++)
            {
             
                int x2 = x+1;
                int y2 = y+1;

                int idx0 = (x * DIM) + y;
                int indexRight = (x2 * DIM) + y;
                int indexUp = (x * DIM) + y2;
                int indexUpRight = (x2 * DIM) + y2;

                Point pi;
                Point pj;
                Point q;
                float vi;
                float vj;

                //lower edge
                pi = Point(x, y);
                pj = Point(x2,y);
                vi = rho[idx0];
                vj = rho[indexRight];
              
                if (scalarBetween(v, vi, vj))
                {
                    q = (pi * (vj - v) + pj * (v - vi)) / (vj - vi);
      
                 
                    isoline.push_back(q);
                }

                //right edge
                pi = Point(x2,y);
                pj = Point(x2,y2);
                vi = rho[indexRight];
                vj = rho[indexUpRight];

                if (scalarBetween(v, vi, vj))
                {
                    q = (pi * (vj - v) + pj * (v - vi)) / (vj - vi);
          
                    isoline.push_back(q);
                }

                //top edge
                pi = Point(x2, y2);
                pj = Point(x,y2);
                vi = rho[indexUpRight];
                vj = rho[indexUp];

                if (scalarBetween(v, vi, vj))
                {
                    q = (pi * (vj - v) + pj * (v - vi)) / (vj - vi);
                    cnt++;
                    isoline.push_back(q);
                }

                //left edge
                pi = Point(x, y2);
                pj = Point(x, y);
                vi = rho[indexUp];
                vj = rho[idx0];

                if (scalarBetween(v, vi, vj))
                {
                    q = (pi * (vj - v) + pj * (v - vi)) / (vj - vi);
                   
                    isoline.push_back(q);
                }           
            }
        }
        isolines.push_back(isoline);
    }
}

void drawIsolines()
{
     vector<float> isovalues;
    
    if(g_drawrange)
    {
        float incr;
        // Draw a range of isolines.
        if(g_numisolines == 1)
            // Avoid division by zero.
            incr = (g_isorho1 - isorho0) /2;
        else
            incr = (g_isorho1 - isorho0) / (g_numisolines-1);   
        
        float j = isorho0;
        for(int i = 0;i < g_numisolines;i++)
        {
            isovalues.push_back(j);
            j+=incr;
        }
    }
    else
        isovalues.push_back(isorho0);
     
 fftw_real cellWidth = (fftw_real) winWidth / (fftw_real) (DIM + 1); // Grid cell width
    fftw_real cellHeight = (fftw_real) winHeight / (fftw_real) (DIM + 1); // Grid cell heigh 

    CColorMap *map = g_VectorLegend->colorMap();

    
    for (int i = 0; i < isolines.size(); i++)
    {
        vector<Point>isoline = isolines[i];

        for (int j = 0; j < isoline.size(); j += 2)
        {
            Point p1 = isoline[j];
            Point p2 = isoline[j + 1];

            glEnable(GL_TEXTURE_1D);
            float fac = float(map->width)/(float)map->size();
            
			glBindTexture(GL_TEXTURE_1D, map->id());
                        
            glBegin(GL_LINES);

            glTexCoord1f(isovalues[i]/fac);

            glVertex2f(p1.y()*cellWidth+cellWidth, p1.x()*cellHeight+cellHeight);
            glVertex2f(p2.y()*cellWidth+cellWidth, p2.x()*cellHeight+cellHeight);

            glDisable(GL_TEXTURE_1D);
            glEnd();
        }
     
    }
}

void rgb2hsv(float r, float g, float b, float& h, float& s, float& v)
{
    float M = max(r, max(g, b));
    float m = min(r, min(g, b));
    float d = M - m;

    v = M;

    s = (M > 0.0001) ? d / M : 0;

    if (s == 0)
    {
        h = 0;
    }
    else
    {
        if (r == M)
        {
            h = (g - b) / d;
        }
        else if (g == M)
        {
            h = 2 + (b - r) / d;
        }
        else
        {
            h = 4 + (r - g) / d;
        }
        h /= 6;

        if (h < 0)
        {
            h += 1;
        }
    }
}

void hsv2rgb(float h, float s, float v, float& r, float& g, float& b)
{
    int hueCase = (int) (h * 6);
    float frac = 6 * h - hueCase;
    float lx = v * (1 - s);
    float ly = v * (1 - s * frac);
    float lz = v * (1 - s * (1 - frac));

    switch (hueCase)
    {
    case 0:
    case 6: r = v;
        g = lz;
        b = lx;
        break;
    case 1: r = ly;
        g = v;
        b = lx;
        break;
    case 2: r = lx;
        g = v;
        b = lz;
        break;
    case 3: r = lx;
        g = ly;
        b = v;
        break;
    case 4: r = lz;
        g = lx;
        b = v;
        break;
    case 5: r = v;
        g = lx;
        b = ly;
        break;
    }
}

//rainbow: Implements a color palette, mapping the scalar 'value' to a rainbow color RGB

void rainbow(float value, float* R, float* G, float* B)
{
    const float dx = 0.8;
    if (value < 0) value = 0;
    if (value > 1) value = 1;
    value = (6 - 2 * dx) * value + dx;
    *R = max(0.0, (3 - fabs(value - 4) - fabs(value - 5)) / 2);
    *G = max(0.0, (4 - fabs(value - 2) - fabs(value - 4)) / 2);
    *B = max(0.0, (3 - fabs(value - 1) - fabs(value - 2)) / 2);
}

//direction_to_color: Set the current color by mapping a direction vector (x,y), using
//                    the color mapping method 'method'. If method==1, map the vector direction
//                    using a rainbow colormap. If method==0, simply use the white color

void direction_to_color(float x, float y, int method)
{
    float r, g, b, f;
    if (method)
    {
        f = atan2(y, x) / 3.1415927 + 1;
        r = f;
        if (r > 1) r = 2 - r;
        g = f + .66667;
        if (g > 2) g -= 2;
        if (g > 1) g = 2 - g;
        b = f + 2 * .66667;
        if (b > 2) b -= 2;
        if (b > 1) b = 2 - b;
    }
    else
    {
        r = g = b = 1;
    }
    glColor3f(r, g, b);
}

fftw_real findMax(fftw_real *rho)
{
    fftw_real tempMax = (fftw_real) INT_MIN;

    for (int i = 0; i < DIM * DIM; i++)
    {
        if (i > 2499 || i < 0)
            cout << "i: " << i;

        if (rho[i] > tempMax)
            tempMax = rho[i];
    }

    return tempMax;
}

fftw_real findMin(fftw_real *rho)
{
    fftw_real tempMin = (fftw_real) INT_MAX;

    for (int i = 0; i < DIM * DIM; i++)
    {
        if (rho[i] < tempMin)
            tempMin = rho[i];
    }

    return tempMin;
}

void computeVelocities()
{

    for (int j = 0; j < DIM; j++)
    {
        for (int i = 0; i < DIM; i++)
        {

            int idx0 = (j * DIM) + i;
            velocity[idx0] = sqrt(vx[idx0] * vx[idx0] + vy[idx0] * vy[idx0]);

        }
    }
}

void computeForceFields()
{
    for (int j = 0; j < DIM; j++)
    {
        for (int i = 0; i < DIM; i++)
        {

            int idx0 = (j * DIM) + i;
            forcefield[idx0] = sqrt(fx[idx0] * fx[idx0] + fy[idx0] * fy[idx0]);
        }
    }
}

void computeDivergenceFields()
{
    //	cout<<"hier";
    //	cout<<"divergence{";
    for (int j = 0; j < DIM; j++)
    {
        for (int i = 0; i < DIM; i++)
        {
            int idx0 = (j * DIM) + i;
            int indexXLeft = (positive_modulo((j - 1), DIM) * DIM) + i;
            int indexXRight = (positive_modulo((j + 1), DIM) * DIM) + i;
            int indexYUp = (j * DIM) + positive_modulo((i + 1), DIM);
            int indexYDown = (j * DIM) + positive_modulo((i - 1), DIM);

            switch (dataset)
            {
            case VELOCITY_DIV_FIELD:
                divergencefield[idx0] = (vx[indexXRight] - vx[indexXLeft]) / 2 + (vy[indexYUp] - vy[indexYDown]) / 2;
                break;
            case FORCE_DIV_FIELD:
                divergencefield[idx0] = (fx[indexXRight] - fx[indexXLeft]) / 2.0 + (fy[indexYUp] - fy[indexYDown]) / 2.0;
                break;
            default:
                break;
            }


            //			cout<<" "<<divergencefield[idx0];
        }
    }
    //	cout<<"}";
    //	cout<<"hier";
}

int positive_modulo(int i, int n)
{
    return (i % n + n) % n;
}

float clamp(float val, float low, float high)
{    
    if(val < low)
        val = low;
    else if(val > high)
        val = high;
    
    return val;
}


//visualize: This is the main visualization function

void visualize(void)
{
    int i, j, idx1, idx2, idx3, idx0;
    double px0, py0, px1, py1, px2, py2, px3, py3;
    fftw_real wn = (fftw_real) winWidth / (fftw_real) (DIM + 1); // Grid cell width
    fftw_real hn = (fftw_real) winHeight / (fftw_real) (DIM + 1); // Grid cell heigh      
    fftw_real max1 = SCALE_MAX;
    fftw_real min1 = SCALE_MIN;

    //  fftw_real *data = (fftw_real *) malloc(sizeof (fftw_real) * DIM * DIM);
    fftw_real *data;
    fftw_real *data3d;
    float aspect = 0.0;
    float F = 0.0;
    float sc = 0.0;       
    
    
    float max3d;
    float min3d;
    
    if(height_plots)
    {                
       // glTranslatef(-14.0,-14.0, -52.0);
        aspect = float(winWidth) / winHeight;
        F = (g_fov / 2.0) * M_PI / 180.0;
        sc = tan(F);
        sc *= g_3ddepth;
        wn = (sc * 2.0 * aspect) / (fftw_real) (DIM + 1);
        hn = (sc * 2.0) / (fftw_real) (DIM + 1);		

		//glRotatef(1.0, 0.0, 1.0, 0.0);
			
    }


 
    
    
    switch (dataset)
    {
    case FLUID_DENSITY:
        data = rho;
        break;
    case FLUID_VELOCITY:
        computeVelocities();
        data = velocity;
        break;
    case FORCE_FIELD:
        computeForceFields();
        data = forcefield;
        break;
    case VELOCITY_DIV_FIELD:
        computeDivergenceFields();
        data = divergencefield;
        break;
    case FORCE_DIV_FIELD:
        computeDivergenceFields();
        data = divergencefield;
        break;
    default:
        break;
    }     
    

    switch (g_dataset_3d)
    {
    case FLUID_DENSITY:
        data3d = rho;
        break;
    case FLUID_VELOCITY:
        computeVelocities();
        data3d = velocity;
        break;
    case FORCE_FIELD:
        computeForceFields();
        data3d = forcefield;
        break;    
    }  
        

    if (relativeScale)
    {
        max1 = findMax(data);
        min1 = findMin(data);
    }  

	
    

    if(height_plots && g_3dscalemode == 1)
    {
        // pre compute height plots dataset.
        // Compute min and max.
        max3d = findMax(data3d);
        min3d = findMin(data3d);
    }

    else
    {				
        for(int i = 0;i < DIM * DIM;i++)
        {		
            data[i] = clamp(data[i], SCALE_MIN, SCALE_MAX);						
        }
    }




    if (draw_smoke)
    {
        CColorMap *map = g_SmokeLegend->colorMap();
        
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glEnable(GL_TEXTURE_1D);
        glBindTexture(GL_TEXTURE_1D, map->id());
        glBegin(GL_TRIANGLES);        

        for (j = 0; j < DIM - 1; j++) //draw smoke
        {
            for (i = 0; i < DIM - 1; i++)
            {
                px0 = wn + (fftw_real) i * wn - (sc * aspect);
                py0 = hn + (fftw_real) j * hn - sc;
                idx0 = (j * DIM) + i;
                px1 = wn + (fftw_real) i * wn - (sc * aspect);
                py1 = hn + (fftw_real) (j + 1) * hn - sc;
                idx1 = ((j + 1) * DIM) + i;
                px2 = wn + (fftw_real) (i + 1) * wn - (sc * aspect);
                py2 = hn + (fftw_real) (j + 1) * hn - sc;
                idx2 = ((j + 1) * DIM) + (i + 1);
                px3 = wn + (fftw_real) (i + 1) * wn - (sc * aspect);
                py3 = hn + (fftw_real) j * hn - sc;
                idx3 = (j * DIM) + (i + 1);
              
                float val = max1 - min1;
				float fac = 1;// float(map->width) / (float)map->size();
                
                float low = 0.0;
                float high = low + g_high;
                if (fabs(max1 - min1) != 0)
                {
                    float zvals[4];
                    memset(zvals, 0, sizeof(float)*4);
                    
                    if(height_plots)
                    {
                        if(g_3dscalemode == 0)
                        {
                            // Clamp mode.
                            zvals[0] = clamp(data3d[idx0]* g_clampscale3d, -g_scalelimit, g_scalelimit);
                            zvals[1] = clamp(data3d[idx1]* g_clampscale3d, -g_scalelimit, g_scalelimit);
                            zvals[2] = clamp(data3d[idx2]* g_clampscale3d, -g_scalelimit, g_scalelimit);
                            zvals[3] = clamp(data3d[idx3]* g_clampscale3d, -g_scalelimit, g_scalelimit);
                        }
                        
                        if(g_3dscalemode == 1)
                        {
                           //  Relative scale mode.   
                            zvals[0] = (data3d[idx0] - min3d) * (((high -low)/(max3d-min3d))) + low;
                            zvals[1] = (data3d[idx1] - min3d) * (((high -low)/(max3d-min3d))) + low;
                            zvals[2] = (data3d[idx2] - min3d) * (((high -low)/(max3d-min3d))) + low;
                            zvals[3] = (data3d[idx3] - min3d) * (((high -low)/(max3d-min3d))) + low;
                            
                           // zvals[0] = (data3d[idx0] - min1) * (((high -low)/(max1-min1))) + low;
                           // zvals[1] = (data3d[idx1] - min1) * (((high -low)/(max1-min1))) + low;
                            //zvals[2] = (data3d[idx2] - min1) * (((high -low)/(max1-min1))) + low;
                           // zvals[3] = (data3d[idx3] - min1) * (((high -low)/(max1-min1))) + low;
                        }
                    }    
                    
                 
                    // Clamp the data values between min and max (only applies to absolute scaling.)
                    //data[idx0] = clamp(data[idx0], SCALE_MIN, SCALE_MAX);
                   // data[idx1] = clamp(data[idx1], SCALE_MIN, SCALE_MAX);
                  //  data[idx2] = clamp(data[idx2], SCALE_MIN, SCALE_MAX);
                   // data[idx3] = clamp(data[idx3], SCALE_MIN, SCALE_MAX);
                    

                    glTexCoord1f((((data[idx0] - min1) / fabs(val)))/(float)fac);

                    glVertex3f(px0, py0, zvals[0]);

                   
                    glTexCoord1f((data[idx1] - min1) / fabs(val)/(float)fac);
                    glVertex3f(px1, py1, zvals[1]);
                 
                    glTexCoord1f(((data[idx2] - (min1)) / fabs(val))/(float)fac);
                    glVertex3f(px2, py2, zvals[2]);

                    glTexCoord1f(((data[idx0] - (min1)) / fabs(val))/(float)fac);
                    glVertex3f(px0, py0, zvals[0]);

                    glTexCoord1f(((data[idx2] - (min1)) / fabs(val))/(float)fac);
                    glVertex3f(px2, py2, zvals[2]);

                    glTexCoord1f(((data[idx3] - (min1)) / fabs(val))/(float)fac);
                    glVertex3f(px3, py3, zvals[3]);
                }
            }
        }
        glDisable(GL_TEXTURE_1D);
        glEnd();
        
    }

    idx0 = 0;
    if (draw_vecs)
    {
        drawVectors(glyphtype, max1);
    }
    //	printf("drawil: %d", draw_isolines);
    if (draw_isolines)
    {
     //   printf("compute and draw");
        computeIsolines();
        drawIsolines();
    }
    if(height_plots)
    {
        //glTranslatef(14.0,14.0, 52.0);
    }

}

//
// drawText
// Draws some text at the given positions using a GLUT_BITMAP_9_BY_15 font.
//

void drawText(const char *text, int length, int x, int y)
{
    glMatrixMode(GL_PROJECTION); // change the current matrix to PROJECTION
    double matrix[16]; // 16 doubles in stack memory
    glGetDoublev(GL_PROJECTION_MATRIX, matrix); // get the values from PROJECTION matrix to local variable
    glLoadIdentity(); // reset PROJECTION matrix to identity matrix
    gluOrtho2D(0.0, (GLdouble) winWidth, 0.0, (GLdouble) winHeight);
    glMatrixMode(GL_MODELVIEW); // change current matrix to MODELVIEW matrix again
    glLoadIdentity(); // reset it to identity matrix
    glPushMatrix(); // push current state of MODELVIEW matrix to stack
    glLoadIdentity(); // reset it again. (may not be required, but it my convention)
    glRasterPos2i(x, y); // raster position in 2D
    for (int i = 0; i < length; i++)
    {
        glutBitmapCharacter(GLUT_BITMAP_9_BY_15, (int) text[i]); // generation of characters in our text with 9 by 15 GLU font
    }
    glPopMatrix(); // get MODELVIEW matrix value from stack
    glMatrixMode(GL_PROJECTION); // change current matrix mode to PROJECTION
    glLoadMatrixd(matrix); // reset
    glMatrixMode(GL_MODELVIEW); // change current matrix mode to MODELVIEW
}

float stringWidth(std::string str)
{
    return 9 * str.length();
}

float getScaleMax(int type)
{
    fftw_real *set;

    if (relativeScale)
    {
    	if(type <= 1){
			if (dataset == FLUID_DENSITY)
			{
				set = rho;
			}
			else if (dataset == FLUID_VELOCITY)
			{
				set = velocity;
			}
			else if (dataset == FORCE_FIELD)
			{
				set = forcefield;
			}
			else if (dataset == VELOCITY_DIV_FIELD)
			{
				set = divergencefield;
			}
			else if (dataset == FORCE_DIV_FIELD)
			{
				set = divergencefield;
			}
    	}else{
    		if (scalar_field == FLUID_DENSITY)
			{
				set = rho;
			}
			else if (scalar_field == FLUID_VELOCITY)
			{
				set = velocity;
				computeVelocities();
			}
			else if (scalar_field == FORCE_FIELD)
			{
				computeForceFields();
				set = forcefield;
			}
    	}

        return (float) findMax(set);
    }

    return SCALE_MAX;
}

float getScaleMin(int type)
{
    fftw_real *set;

    if (relativeScale)
    {
    	if(type <= 1){
			if (dataset == FLUID_DENSITY)
			{
				set = rho;
			}
			else if (dataset == FLUID_VELOCITY)
			{
				set = velocity;
			}
			else if (dataset == FORCE_FIELD)
			{
				set = forcefield;
			}
			else if (dataset == VELOCITY_DIV_FIELD)
			{
				set = divergencefield;
			}
	
			else if (dataset == FORCE_DIV_FIELD)
			{
				set = divergencefield;
			}
    	}else{
    		if (scalar_field == FLUID_DENSITY)
			{
				set = rho;
			}
			else if (scalar_field == FLUID_VELOCITY)
			{
				computeVelocities();
				set = velocity;
			}
			else if (scalar_field == FORCE_FIELD)
			{
				computeForceFields();
				set = forcefield;
			}
    	}


        return (float) findMin(set);
    }

    return SCALE_MIN;
}


