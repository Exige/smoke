#ifndef _VISUALIZATION_H_
#define _VISUALIZATION_H_

//
// This file contains all the visualization definitions.
//

#include "simulation.h"
#include "point.h"
#include <climits>
#include <vector>
#include <iostream>
#include <set>
#include<utility>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include "simulation.h"
#include "CColorMap.h"

using namespace std;


extern int   winWidth, winHeight;      //size of the graphics window, in pixels
extern int   color_dir;            //use direction color-coding or not
extern float vec_scale;			//scaling of hedgehogs
extern int   draw_smoke;           //draw the smoke or not
extern int   draw_vecs;            //draw the vector field or not
extern int 	draw_isolines;
extern int   scalar_col;           //method for scalar coloring
extern int   scalar_col_vec;           //method for scalar coloring
extern float SCALE_MAX;   // Max scale for RHO.
extern float SCALE_MIN;   // Max scale for RHO.
extern int relativeScale;
extern int g_drawrange;

extern float hue;
extern float saturation;

extern int vec_dim_x;
extern int vec_dim_y;

// Velocity and Forcefield scalars.
extern fftw_real *forcefield;
extern fftw_real *divergencefield;
extern fftw_real *velocity;

extern const int FLUID_DENSITY;
extern const int FLUID_VELOCITY;
extern const int FORCE_FIELD;
extern const int VELOCITY_DIV_FIELD;
extern const int FORCE_DIV_FIELD;

// Enable height plots?
extern int height_plots;

// isolines values
extern float isorho0;
extern float g_isorho1;
extern int g_numisolines;

// Fov.
extern float g_fov;
extern float g_3ddepth;
extern int g_dataset_3d;

extern int g_3dscalemode;
// Scale clamping limits for clamp height plot mode.
extern float g_scalelimit;

// Scale factor (clamping).)
extern float g_clampscale3d;
extern float g_high;

// Moving the height plot.
extern int g_movemode;

// Zooming in height plot mode.
extern int g_zoomfactor;




void hsv2rgb(float h, float s, float v, float& r, float& g, float& b);
void rgb2hsv(float r, float g, float b, float& h, float& s, float& v);
void rainbow(float value,float* R,float* G,float* B);
void direction_to_color(float x, float y, int method);
void visualize(void);
void drawText(const char *text, int length, int x, int y);
float stringWidth(std::string str);
float getScaleMax(int type);
fftw_real findMax(fftw_real *rho);

float getScaleMin(int type);

int positive_modulo(int i, int n) ;
void setVideoMode();


#endif
