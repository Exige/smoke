/* 
 * File:   glyphs.h
 * Author: s2418657
 *
 * Created on December 2, 2014, 10:18 AM
 */

#ifndef GLYPHS_H
#define	GLYPHS_H

#include "fftw.h"

void drawVectors(int glyphtype, fftw_real max); 

extern int glyphtype;

enum EGlyphType
{
    Glyph_Lines,
    Glyph_Triangles,
    Glyph_Arrows,
};

#endif	/* GLYPHS_H */

