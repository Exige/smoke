#ifndef SIMULATION_H
#define SIMULATION_H

//#include <windows.h>
#include <rfftw.h>              //the numerical simulation FFTW library
#include <stdio.h>              //for printing the help text
#include <math.h>               //for various math functions
#include <GL/glut.h>            //the GLUT graphics library
#include <GL/glui.h>

//--- SIMULATION PARAMETERS ------------------------------------------------------------------------
extern const int DIM;				//size of simulation grid
extern double dt;				//simulation time step
extern float visc;				//fluid viscosity
extern fftw_real *vx, *vy;             //(vx,vy)   = velocity field at the current moment
extern fftw_real *vx0, *vy0;           //(vx0,vy0) = velocity field at the previous moment
extern fftw_real *fx, *fy;	            //(fx,fy)   = user-controlled simulation forces, steered with the mouse
extern fftw_real *rho, *rho0;			//smoke density at the current (rho) and previous (rho0) moment
extern rfftwnd_plan plan_rc, plan_cr;  //simulation domain discretization
extern int   frozen;               //toggles on/off the animation
extern const float RHO_MAX;			// Initial value of rho.


extern int dataset;
extern int scalar_field;
extern int vector_field;

// Main window.
extern int main_window;




//------ SIMULATION CODE STARTS HERE -----------------------------------------------------------------

//init_simulation: Initialize simulation data structures as a function of the grid size 'n'.
//                 Although the simulation takes place on a 2D grid, we allocate all data structures as 1D arrays,
//                 for compatibility with the FFTW numerical library.
void init_simulation(int n);

/***************************************** myGlutIdle() ***********/
void myGlutIdle( void );


//FFT: Execute the Fast Fourier Transform on the dataset 'vx'.
//     'dirfection' indicates if we do the direct (1) or inverse (-1) Fourier Transform
void FFT(int direction,void* vx);

int clamp(float x);

#ifndef WIN32
float max(float x, float y);
#endif

//solve: Solve (compute) one step of the fluid flow simulation
void solve(int n, fftw_real* vx, fftw_real* vy, fftw_real* vx0, fftw_real* vy0, fftw_real visc, fftw_real dt);


// diffuse_matter: This function diffuses matter that has been placed in the velocity field. It's almost identical to the
// velocity diffusion step in the function above. The input matter densities are in rho0 and the result is written into rho.
void diffuse_matter(int n, fftw_real *vx, fftw_real *vy, fftw_real *rho, fftw_real *rho0, fftw_real dt);


//set_forces: copy user-controlled forces to the force vectors that are sent to the solver.
//            Also dampen forces and matter density to get a stable simulation.
void set_forces(void);


//do_one_simulation_step: Do one complete cycle of the simulation:
//      - set_forces:
//      - solve:            read forces from the user
//      - diffuse_matter:   compute a new set of velocities
//      - gluPostRedisplay: draw a new visualization frame
void do_one_simulation_step(void);

#endif /* SIMULATION_H */