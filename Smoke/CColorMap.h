#ifndef _CCOLORMAP_H_
#define  _CCOLORMAP_H_

// gl.h depends on this in windows.
#ifdef WIN32
#include <windows.h>
#endif

#include <vector>
#include <GL/gl.h>

enum EColorMapTypes
{
	COLOR_BLACKWHITE, 
	COLOR_RAINBOW,
	COLOR_ZEBRA,
};

struct rgb
{
	float r;
	float g;
	float b;
};

class CColorMap
{
public:
	CColorMap();
	void set();
	void setColor(float value);
	void generateTexture();
	void setTextureId();
	 
	int size();

	rgb &at(int i);

    GLuint id();
    GLuint textId();

	// Live variables for Glui.
	int type;
	int levels;
        const int width;
private:	
	std::vector<rgb> m_colormap;	
	float m_hue;
	float m_saturation;
	unsigned char* image;
	unsigned char* text;        

    unsigned int m_textureId;
    unsigned int m_textureIdText;
};



#endif //_CCOLORMAP_H_


