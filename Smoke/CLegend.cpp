/* 
 * File:   CLegend.cpp
 * Author: thomas
 * 
 * Created on December 9, 2014, 9:22 PM
 */

#include "CLegend.h"
#include "visualization.h"

CLegend *g_SmokeLegend;
CLegend *g_VectorLegend;

CLegend::CLegend()
{
}

void CLegend::changed()
{
}

CLegend::CLegend(const CLegend& orig)
{
}

CLegend::~CLegend()
{
}

CLegend::CLegend(const char* name)
{
	
    m_name = name;        
//    this->m_colormap.setTextureId();
}

string &CLegend::name()
{
    return m_name;
}



CColorMap *CLegend::colorMap()
{
	return &m_colormap;
}

void CLegend::draw(float x)
{

    // Temporarily switch to glortho2d.
    // change the current matrix to PROJECTION
    glMatrixMode(GL_PROJECTION); 
    
    // get the values from PROJECTION matrix to local variable    
    double projectmatrix[16]; 
    glGetDoublev(GL_PROJECTION_MATRIX, projectmatrix); 
    
    // Reset projection matrix.
    glLoadIdentity();
    
     // change current matrix to MODELVIEW matrix again
    glMatrixMode(GL_MODELVIEW);
    
    // Get the modelview matrix.
    double modelmatrix[16]; 
    glGetDoublev(GL_MODELVIEW_MATRIX, modelmatrix); 
    
    // Reset modelview matrix.
    glLoadIdentity();
    
   // // 2D projection
    gluOrtho2D(0.0, (GLdouble) winWidth, 0.0, (GLdouble) winHeight);
    
   
    
    
    glEnable(GL_TEXTURE_1D);    
    glBindTexture(GL_TEXTURE_1D, this->m_colormap.id());
    glBegin(GL_QUADS);

    // Step size in y position of the color bars.
    float step = (float)(1.0 / (float)m_colormap.size());

    // Draw the color rectangles.
    float fac = m_colormap.width/(float)m_colormap.size();
 
    for (int l = 0; l < m_colormap.size(); l++)
    {
        // Get texture coordinate of this bar.
        glTexCoord1f((float)l / (float)m_colormap.width);

        // Draw vertices.
        glVertex2f(x, (l * step) * winHeight);
        glVertex2f(x + 20.0f, (l * step) * winHeight);

        glVertex2f(x + 20.0f, (l * step) * winHeight + step * winHeight);
        glVertex2f(x, (l * step) * winHeight + step * winHeight);
    }

    glDisable(GL_TEXTURE_1D);
    glEnd();

    // Draw the scale numbers.
    char *str = new char[20];
    memset(str, 0, 20);

    float cor = 20.0;
    if (x > winWidth - stringWidth(m_name))
    {
        cor = -stringWidth(m_name);
    }
    
    glEnable(GL_TEXTURE_1D);
    glBindTexture(GL_TEXTURE_1D, this->m_colormap.textId());
    glTexCoord1f(1);
    drawText(m_name.data(), m_name.length(), x + cor, winHeight - 15);
    glDisable(GL_TEXTURE_1D);


    float scale = getScaleMax(x);
    float scalemin = getScaleMin(x);

    float j = scalemin;
    for (int i = 0; i < 10; i++)
    {
        sprintf(str, "%.3f", j);
        j += fabs(scale - scalemin) / 10;

        glEnable(GL_TEXTURE_1D);
        glBindTexture(GL_TEXTURE_1D, this->m_colormap.textId());
        glTexCoord1f(1);
        if(x == 0){
        	drawText(str, 20, 5+x, float((i / 10.0 * winHeight)));
        }else{
        	drawText(str, 20, 5+x + 0.25*cor, float((i / 10.0 * winHeight)));
        }
        glDisable(GL_TEXTURE_1D);

    }

    delete str;
    
    // Reset our matrices.    
    glMatrixMode(GL_PROJECTION); // change current matrix mode to PROJECTION
   glLoadMatrixd(projectmatrix); // reset
    glMatrixMode(GL_MODELVIEW); // change current matrix mode to MODELVIEW 
   glLoadMatrixd(modelmatrix); // reset
    
}


