class Point {
public:

    float x() {
        return m_x;
    }

    float y() {
        return m_y;
    }

    void setX(float x) {
        m_x = x;
    }

    void setY(float y) {
        m_y = y;
    }

    Point(float x, float y) {
        m_x = x;
        m_y = y;
    }

    Point() {
    }

    Point operator*(float s) const {


        return Point(m_x * s, m_y * s);
    }

    Point operator/(float s) const {
        return operator*(1 / s);
    }

    bool operator<(Point p) const{
		
            return p.x() < m_x;
    }

    Point operator-(Point p1) const {
       return Point(m_x - p1.x(), m_y - p1.y());
    }

    Point operator+(Point p1) const {

        return Point(m_x + p1.x(), m_y + p1.y());

    }

private:
    float m_x;
    float m_y;
};