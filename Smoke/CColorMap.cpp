#include "CColorMap.h"
#include "visualization.h"


CColorMap::CColorMap()
	: width(256)
{	
	m_textureId = -1;
	m_textureIdText = -1;
	m_hue = 0;
	m_saturation = 1.0;
	levels = 255;
	type = COLOR_BLACKWHITE;
	set();
}

int CColorMap::size()
{
	return m_colormap.size();
}

rgb &CColorMap::at(int i)
{
	return m_colormap[i];
}


void CColorMap::set()
{
	m_colormap.clear();	

	float step = 1 / (float) (levels - 1);
	float value = 0;

	for (int i = 0; i < levels; i++) {
		m_colormap.push_back(rgb());

		if (type == COLOR_BLACKWHITE) {
			m_colormap[i].r = value;
			m_colormap[i].g = value;
			m_colormap[i].b = value;
		} else if (type == COLOR_RAINBOW) {

			rainbow(value, &m_colormap[i].r, &m_colormap[i].g, &m_colormap[i].b);

			float h;
			float s;
			float v;

			float r = m_colormap[i].r;
			float g = m_colormap[i].g;
			float b = m_colormap[i].b;

//			printf("start r: %f, g: %f, b: %f\n", r, g, b);

			rgb2hsv(r, g, b, h, s, v);

			h += hue / 360;

			h = fmod(h, 1);      
					
			s *= saturation;

			hsv2rgb(h, s, v, r, g, b);
//			printf("end r: %f, g: %f, b: %f\n", r, g, b);
			m_colormap[i].r = r;
			m_colormap[i].g = g;
			m_colormap[i].b = b;
		} else if (type == COLOR_ZEBRA) {
			if (i % 2 == 0) {
				m_colormap[i].r = 0;
				m_colormap[i].g = 0;
				m_colormap[i].b = 0;
			} else {
				m_colormap[i].r = 1;
				m_colormap[i].g = 1;
				m_colormap[i].b = 1;
			}
		}

		value += step;
	}
	if(id() == -1){
		setTextureId();
	}
	generateTexture();
}

GLuint CColorMap::id()
{
	return m_textureId;
}

GLuint CColorMap::textId()
{
	return m_textureIdText;
}

void CColorMap::setTextureId()
{
	glGenTextures(1, &(this->m_textureId));
	glGenTextures(1, &(this->m_textureIdText));
//	printf("texture id: %d\n", this->id());
//	printf("texture text id: %d\n", this->textId());
	fflush(stdout);
}

void CColorMap::generateTexture()
{
	fflush(stdout);	
	image = new unsigned char[width * 3];
	text = new unsigned char[3];
	int j = 0;       
        
	for(int i = 0; i < m_colormap.size(); i++){
		image[i*3] = m_colormap[i].r * 255;
		image[i*3 + 1] = m_colormap[i].g * 255;
		image[i*3 + 2] = m_colormap[i].b * 255;
                j = i;
	}
        
        for(int i = m_colormap.size();i < width;i++)
        {
            image[i*3] = m_colormap[j].r * 255;
            image[i*3 + 1] = m_colormap[j].g * 255;
            image[i*3 + 2] = m_colormap[j].b * 255;
        }
	
	//set text to red
	text[0] = 255;
	text[1] = 0;
	text[2] = 0;
	
	glBindTexture(GL_TEXTURE_1D, this->id());
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, width, 0,
	  GL_RGB, GL_UNSIGNED_BYTE, image);
	
	//text texture
	glBindTexture(GL_TEXTURE_1D, this->textId());
	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 1, 0,
		  GL_RGB, GL_UNSIGNED_BYTE, text);
        
        delete [] image;
}



void CColorMap::setColor(float value)
{
	if (value > 1)
		value = 1;

	float step = 1 / (float) m_colormap.size();
	float R, G, B = -1;

	for (int i = 0; i < m_colormap.size(); i++) {
		if (value <= (step * (i + 1))) {
			R = m_colormap[i].r;
			G = m_colormap[i].g;
			B = m_colormap[i].b;

			break;
		}
	}

	// Due to rounding errors it may occur that 
	// (step*(i+1)) might not actually equal 1 in the previous for loop
	// This will cause a problem if value is 1
	// So set it here if B still equals -1
	// Sloppy hack, but works for now.

	if (B == -1) {
		R = m_colormap[m_colormap.size() - 1].r;
		G = m_colormap[m_colormap.size() - 1].g;
		B = m_colormap[m_colormap.size() - 1].b;
	}

	glColor3d(R, G, B);
}