// Usage: Drag with the mouse to add smoke to the fluid. This will also move a "rotor" that disturbs
//        the velocity field at the mouse location. Press the indicated keys to change options
//--------------------------------------------------------------------------------------------------

#include <rfftw.h>              //the numerical simulation FFTW library
#include <stdio.h>              //for printing the help text
#include <string>
#include <math.h>               //for various math functions
#include <GL/glut.h>            //the GLUT graphics library
#include <GL/glui.h>
#include "visualization.h"
#include "glyphs.h"
#include "CLegend.h"

//------ INTERACTION CODE STARTS HERE -----------------------------------------------------------------
double g_save[16];

// here we keep the state of the rotation matrix.
double g_rot[16];

//display: Handle window redrawing events. Simply delegates to visualize().
void display(void)
{
    // Make the background white
    glClearColor(1.0, 1.0, 1.0, 1.0);         
   
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);	
    visualize();
	
    g_SmokeLegend->draw(0);
    
// Draw the vector legend if vectors or isolines are enabled.
if (draw_vecs || draw_isolines)
g_VectorLegend->draw(winWidth - 20);

glFlush();
glutSwapBuffers();
}

void setVideoMode()
{
	if (height_plots)
	{
		// Set up 3d view for height plots.
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		// Place the camera.
		gluLookAt(0.0, 0.0, 100.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

		// Save the initial state of the modelview matrix. 
		// This is needed for rotation.
		glGetDoublev(GL_MODELVIEW_MATRIX, g_save);

		// Also, reset the rotation matrix itself with the identity matrix.
		double identity[16] =
		{
			1, 0, 0, 0,
			0, 1, 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		memcpy(g_rot, identity, sizeof(double) * 16);

		// Set up perspective
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(g_fov, (GLfloat)winWidth / (GLfloat)winHeight, 0.1f, 200.0f);
	}
	else
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		// Set up 2d projection view.
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0.0, (GLdouble)winWidth, 0.0, (GLdouble)winHeight);
		int tx, ty, tw, th;
		GLUI_Master.get_viewport_area(&tx, &ty, &tw, &th);
		glViewport(tx, ty, tw, th);
	}
}

//reshape: Handle window resizing (reshaping) events
void reshape(int w, int h)
{
	int tx, ty, tw, th;
	// glViewport(0.0f, 0.0f, (GLfloat)w, (GLfloat)h);

	// Get viewport area from glui.
	GLUI_Master.get_viewport_area(&tx, &ty, &tw, &th);
	glViewport(tx, ty, tw, th);

	// Set global window width and height.
	winWidth = tw;
	winHeight = th;

	// Set up camera and perspective.
	setVideoMode();
}

//keyboard: Handle key presses
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 't': dt -= 0.001; break;
	case 'T': dt += 0.001; break;
	case 'c': color_dir = 1 - color_dir; break;
	case 'S': vec_scale *= 1.2; break;
	case 's': vec_scale *= 0.8; break;
	case 'V': visc *= 5; break;
	case 'v': visc *= 0.2; break;
	case 'x': draw_smoke = 1 - draw_smoke;
		if (draw_smoke == 0) draw_vecs = 1; break;
	case 'y': draw_vecs = 1 - draw_vecs;
		if (draw_vecs == 0) draw_smoke = 1; break;
	case 'm': scalar_col++; if (scalar_col > COLOR_ZEBRA) scalar_col = COLOR_BLACKWHITE; break;
	case 'a': frozen = 1 - frozen; break;
	case 'q': exit(0);
	}
}
int lmx = 0, lmy = 0;

bool valid;
int g_oldzoom;



void mouse(int button, int state, int x, int y)
{	
	if (state == GLUT_UP)
	{
		// Be sure to reset mouse position for when moving the height plot.
		valid = false;
	}	
}

void matrixmult(double *v, double **m, double *r)
{
	for (int i = 0; i < 3; i++)
	{
		double s = 0;
		for (int j = 0; j < 3; j++)
		{
			double a = m[j][i];
			s += v[j] * a;
		}
		r[i] = s;
	}
}

// drag: When the user drags with the mouse, add a force that corresponds to the direction of the mouse
//       cursor movement. Also inject some new matter into the field at the mouse location.
void drag(int mx, int my)
{	
	// GLfloat wx=mx,wy, wz;
	int xi, yi, X, Y; double  dx, dy, len;	
	float vec[2] = { 1.0, 1.0 };
	if (height_plots && g_movemode == 1)
	{		
		if (!valid)
		{
			lmx = mx;
			lmy = my;
			valid = true;
			return;			
		}
		// Move mode in height plots view, move the plane instead.
		int changex = mx - lmx;
		int changey = my - lmy;
		
		if (changex < 0)
		{
			vec[0] = -vec[0];			
		}
		if (changey < 0)
		{
			vec[1] = -vec[1];
		}

		changex = abs(changex);
		changey = abs(changey);

		glLoadIdentity();
		
		glRotatef((float)changey / 10.0, vec[1], 0.0, 0.0);
		glRotatef((float)changex / 10.0, 0.0, vec[0], 0.0);

		glMultMatrixd(g_rot);
		glGetDoublev(GL_MODELVIEW_MATRIX, g_rot);

		glLoadMatrixd(g_save);		

		glMultMatrixd(g_rot);

		lmx = mx; lmy = my;
		return;
	}

//GLdouble ox=0.0,oy=0.0,oz=0.0;
       // GLint viewport[4];
      //  GLdouble modelview[16],projection[16];
       // glGetDoublev(GL_MODELVIEW_MATRIX,modelview);
        //    glGetDoublev(GL_PROJECTION_MATRIX,projection);
        //    glReadPixels(mx,my,1,1,GL_DEPTH_COMPONENT,GL_FLOAT,&wz);
          //  glGetIntegerv(GL_VIEWPORT,viewport);
     //my = (winHeight - my) / (double)winHeight;     
  //  my=viewport[3]-my;
    //wy=my;
   // wx /= (mx / (double)winWidth);
         //   gluUnProject(wx,wy,wz,modelview,projection,viewport,&ox,&oy,&oz);
            
            //wx = ox; wy = oy; wz = oz;
            
           // GLdouble aa = (double)(DIM + 1);
            //gluUnProject(wx,wy,wz,modelview,projection,viewport,&ox,&oy,&oz);
            
	// Compute the array index that corresponds to the cursor location
	xi = (int)clamp((double)(DIM + 1) * ((double)mx / (double)winWidth));
	yi = (int)clamp((double)(DIM + 1) * ((double)(winHeight - my) / (double)winHeight));
                 

	X = xi; Y = yi;

	if (X > (DIM - 1))  X = DIM - 1; if (Y > (DIM - 1))  Y = DIM - 1;
	if (X < 0) X = 0; if (Y < 0) Y = 0;

	// Add force at the cursor location
	my = winHeight - my;
	dx = mx - lmx; dy = my - lmy;
	len = sqrt(dx * dx + dy * dy);
	if (len != 0.0) {  dx *= 0.1 / len; dy *= 0.1 / len; }
	fx[Y * DIM + X] += dx;
	fy[Y * DIM + X] += dy;
	rho[Y * DIM + X] = RHO_MAX;
	lmx = mx; lmy = my;
}

void zoomchanged(int)
{
	GLdouble modelview[16];
	glGetDoublev(GL_MODELVIEW_MATRIX,modelview);

	glLoadIdentity();

	glTranslatef(0.0, 0.0, -g_oldzoom);
	glTranslatef(0.0, 0.0, g_zoomfactor);

	glMultMatrixd(g_save);
	glGetDoublev(GL_MODELVIEW_MATRIX, g_save);

	glMultMatrixd(g_rot);
	
	g_oldzoom = g_zoomfactor;

	//glMultMatrixd(modelview);

	// Save the matrix for the rotation.
//	glGetDoublev(GL_MODELVIEW_MATRIX, g_save);
}



void colormapChanged(int type)
{
	if(type == 1)
		g_SmokeLegend->colorMap()->set();
	else if(type == 2)
		g_VectorLegend->colorMap()->set();

}

void isolinesChanged(int){

}

void viewChanged(int){
    setVideoMode();
}

//main: The main program
int main(int argc, char **argv)
{
	g_movemode = 0;
	g_zoomfactor = 1.0;
	g_oldzoom = 0.0;
	printf("Fluid Flow Simulation and Visualization\n");
	printf("=======================================\n");
	printf("Click and drag the mouse to steer the flow!\n");
	printf("T/t:   increase/decrease simulation timestep\n");
	printf("S/s:   increase/decrease hedgehog scaling\n");
	printf("c:     toggle direction coloring on/off\n");
	printf("V/v:   increase decrease fluid viscosity\n");
	printf("x:     toggle drawing matter on/off\n");
	printf("y:     toggle drawing hedgehogs on/off\n");
	printf("m:     toggle thru scalar coloring\n");
	printf("a:     toggle the animation on/off\n");
	printf("q:     quit\n\n");	

	double v1[3] = { 1, 2, 3 };
	double m1[3][3] =
	{
		{1.0, 2.0, 3.0},
		{4.0, 5.0, 6.0},
		{7.0, 8.0, 9.0},
	};

	double v2[3];
	double **m2;
	m2 = new double*[3];
	for (int i = 0; i < 3; i++)
	{
		m2[i] = new double[3];		
		for (int j = 0; j < 3; j++)
		{
			m2[i][j] = m1[i][j];
		}
		int a1 = m2[i][0];
		a1 = m2[i][1];
		a1 = m2[i][2];
		a1 = m2[i][1];
	}

	

	
	// Reserve memory for the arrays the hold the forcefields and velocities.
	velocity = new fftw_real[DIM*DIM];
	forcefield = new fftw_real[DIM*DIM];
	divergencefield = new fftw_real[DIM*DIM];
        
        g_fov = 60.0;
        g_3ddepth = 70.0;
        g_scalelimit = 60;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(700,700);
	main_window = glutCreateWindow("Real-time smoke simulation and visualization");
	glutDisplayFunc(display);

	// Initialize legends.
	g_SmokeLegend = new CLegend("Smoke legend");
	g_VectorLegend = new CLegend("Vector Legend");
	fflush(stdout);
	//	glutReshapeFunc(reshape);
	//	glutIdleFunc(do_one_simulation_step);
	glutKeyboardFunc(keyboard);
	glutMotionFunc(drag);
	glutMouseFunc(mouse);

	//glui code
	GLUI *glui = GLUI_Master.create_glui_subwindow( main_window, GLUI_SUBWINDOW_RIGHT );


	GLUI_Panel *fluid_panel = glui->add_rollout( "Fluid parameters" );

	glui->add_spinner_to_panel(fluid_panel, "Scale max", GLUI_SPINNER_FLOAT, &SCALE_MAX);
	glui->add_spinner_to_panel(fluid_panel, "Scale min", GLUI_SPINNER_FLOAT, &SCALE_MIN);

	glui->add_checkbox_to_panel(fluid_panel, "Freeze", &frozen);
	glui->add_checkbox_to_panel(fluid_panel, "Relative scale", &relativeScale);

	GLUI_Listbox *listbox2 = glui->add_listbox_to_panel(fluid_panel, "Data set", &dataset, 1, &colormapChanged);
	listbox2->add_item(0,"fluid density (rho)");
	listbox2->add_item(1,"fluid velocity (||v||)");
	listbox2->add_item(2,"force field (||f||)");
	listbox2->add_item(3,"velocity divergence");
	listbox2->add_item(4,"force divergence");


	GLUI_Panel *color_panel = glui->add_rollout( "Color parameters" );

	GLUI_Listbox *listbox = glui->add_listbox_to_panel(color_panel, "Colormap", &g_SmokeLegend->colorMap()->type, 1, &colormapChanged);
	listbox->add_item(0,"Grayscale");
	listbox->add_item(1,"Rainbow");
	listbox->add_item(2,"Zebra");
//	listbox->add_item(3,"Winter");

	glui->add_spinner_to_panel(color_panel,"Colorband range", GLUI_SPINNER_INT, &g_SmokeLegend->colorMap()->levels, 1, &colormapChanged);

	glui->add_spinner_to_panel(color_panel,"Hue", GLUI_SPINNER_FLOAT, &hue, 1, &colormapChanged);

	GLUI_Spinner *satPointer = glui->add_spinner_to_panel(color_panel,"Saturation", GLUI_SPINNER_FLOAT, &saturation, 1, &colormapChanged);
	satPointer->set_float_limits(0,1,GLUI_LIMIT_CLAMP);


	GLUI_Panel *vector_panel = glui->add_rollout( "Vector parameters" );
	glui->add_checkbox_to_panel(vector_panel, "Draw vectors", &draw_vecs);

	GLUI_Listbox *listbox4 = glui->add_listbox_to_panel(vector_panel, "Scalar field", &scalar_field, 2, &colormapChanged);
	listbox4->add_item(0,"fluid density (rho)");
	listbox4->add_item(1,"fluid velocity (||v||)");
	listbox4->add_item(2,"force field (||f||)");

	GLUI_Listbox *listbox5 = glui->add_listbox_to_panel(vector_panel, "Vector field", &vector_field, 2, &colormapChanged);
	listbox5->add_item(0,"fluid velocity");
	listbox5->add_item(1,"force field");

	GLUI_Listbox *listbox3 = glui->add_listbox_to_panel(vector_panel, "Glyph type", &glyphtype, 0);
	listbox3->add_item(0,"Lines");
	listbox3->add_item(1,"Triangles");
	listbox3->add_item(2,"Arrows");

	glui->add_spinner_to_panel(vector_panel, "Vector x dimensions", GLUI_SPINNER_INT, &vec_dim_x, 50);
	glui->add_spinner_to_panel(vector_panel, "Vector y dimensions", GLUI_SPINNER_INT, &vec_dim_y, 50);

	GLUI_Listbox *listbox6 = glui->add_listbox_to_panel(vector_panel, "Colormap", &g_VectorLegend->colorMap()->type, 2, &colormapChanged);
	listbox6->add_item(0,"Grayscale");
	listbox6->add_item(1,"Rainbow");
//	listbox6->add_item(2,"Zebra");	
      
	GLUI_Panel *isolines_panel = glui->add_rollout( "Isolines parameters" );
	
	glui->add_checkbox_to_panel(isolines_panel, "Draw isolines", &draw_isolines, 0, &isolinesChanged);
	glui->add_checkbox_to_panel(isolines_panel, "Isoline range", &g_drawrange, 0);
	glui->add_spinner_to_panel(isolines_panel,"rho0", GLUI_SPINNER_FLOAT, &isorho0, 0.0, &isolinesChanged);
	glui->add_spinner_to_panel(isolines_panel,"rho1", GLUI_SPINNER_FLOAT, &g_isorho1, 0.0, &isolinesChanged);
        glui->add_spinner_to_panel(isolines_panel,"Number of isolines", GLUI_SPINNER_INT, &g_numisolines, 0.0, &isolinesChanged);   
        
        GLUI_Panel *heightplots_panel = glui->add_rollout( "Height plots" );
        glui->add_checkbox_to_panel(heightplots_panel, "Enable height plot view", &height_plots, 0.0, &viewChanged);
		glui->add_checkbox_to_panel(heightplots_panel, "Move mode", &g_movemode);
		glui->add_spinner_to_panel(heightplots_panel, "Zoom", GLUI_SPINNER_INT, &g_zoomfactor, 0, zoomchanged);
        GLUI_Listbox *listbox7 = glui->add_listbox_to_panel(heightplots_panel, "Scaling mode", &g_3dscalemode, 0);
	listbox7->add_item(0,"Clamp");
	listbox7->add_item(1,"Scale");
        
        GLUI_Listbox *listbox8 = glui->add_listbox_to_panel(heightplots_panel, "Scalar field", &g_dataset_3d, 1);
	listbox8->add_item(0,"fluid density (rho)");
	listbox8->add_item(1,"fluid velocity (||v||)");
	listbox8->add_item(2,"force field (||f||)");
        g_scalelimit = 70.0;
        g_clampscale3d = 60.0;
        g_high = 50.0;
        glui->add_spinner_to_panel(heightplots_panel, "clamping limit", GLUI_SPINNER_FLOAT, &g_scalelimit);
        glui->add_spinner_to_panel(heightplots_panel, "Clamp Scale factor", GLUI_SPINNER_FLOAT, & g_clampscale3d);       
	glui->add_spinner_to_panel(heightplots_panel, "Scaling mode high value", GLUI_SPINNER_FLOAT, & g_high);   
        
        
	glui->set_main_gfx_window( main_window );

	/* We register the idle callback with GLUI, *not* with GLUT */
	GLUI_Master.set_glutIdleFunc( do_one_simulation_step ); 
	GLUI_Master.set_glutReshapeFunc( reshape );
	// GLUI_Master.auto_set_viewport();
        

	init_simulation(DIM);	//initialize the simulation data structures
	glutMainLoop();			//calls do_one_simulation_step, keyboard, display, drag, reshape
	return 0;
}
