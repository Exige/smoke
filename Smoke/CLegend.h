/* 
 * File:   CLegend.h
 * Author: thomas
 *
 * Created on December 9, 2014, 9:22 PM
 */

#ifndef CLEGEND_H
#define CLEGEND_H    

#include "visualization.h"
#include "CColorMap.h"

class CLegend 
{
public:
    CLegend();
    CLegend(const char *name);
    CLegend(const CLegend& orig);
    virtual ~CLegend();
    
    string &name();
    
    void draw(float x);

	void changed();
	CColorMap *colorMap();	
private:
	CColorMap m_colormap;
    string m_name; 
};

extern CLegend *g_SmokeLegend;
extern CLegend *g_VectorLegend;

#endif	/* CLEGEND_H */

